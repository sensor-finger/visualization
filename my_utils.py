"""Useful utils used across all modules"""

import json
from math import radians
from pathlib import Path

from my_definitions import JOINTS_CONFIG


def load_xml_file(xml_file: Path) -> str:
    """
    Load xml model from file

    :param xml_file: Path to xml file with model
    :type xml_file: Path
    :return: xml string
    :rtype: str
    """
    with open(xml_file, "r", encoding="utf-8") as file:
        return file.read()


def load_joints_config(config: Path = JOINTS_CONFIG) -> dict:
    """
    Load joints configuration from JSON file

    :param config: Path to configuration file, defaults to JOINTS_CONFIG
    :type config: Path, optional
    :return: Joints configuration
    :rtype: dict
    """
    with open(config, "r", encoding="utf-8") as file:
        return json.load(file)


def get_joints_indices(sim) -> dict[str, int]:
    """
    Get all joints indices in MuJoCo sim

    :param sim: MuJoCo simulation
    :type sim: MjSim
    :return:Joint indices in dictionary style
    :rtype: dict[str, int]
    """
    data = {}
    names: list[str] = load_joints_config()["names"]
    for name in names:
        data[name] = sim.model.get_joint_qpos_addr(name)
    return data


def calculate_joints_values(data: dict[str, float]) -> dict[str, float]:
    """Calculate all joints values from only joint containing encoders

    Args:
        data (dict[str, float]): Values of joints with encoders

    Returns:
        dict[str, float]: Calculated values of all joints
    """
    config = load_joints_config()
    signs = config["signs"]
    calculations = config["calculations"]
    scale = config["scale"]

    values = {}
    for name, value in data.items():
        values[name] = radians(signs[name] * value) * scale
        if name != "JOINT1E":
            for child_name, child_scale in calculations[name].items():
                values[child_name] = (
                    radians(signs[child_name] * value * child_scale) * scale
                )

    return values
