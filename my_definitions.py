"""Definitions used in all modules"""

from pathlib import Path

ROOT_DIR = Path(__file__).parent
MODEL_XML = ROOT_DIR / "paluch.xml"
JOINTS_CONFIG = ROOT_DIR / "joints_config.json"
