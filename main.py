#!/usr/bin/env python3

"""Module docstring
"""

import signal

import zmq
from mujoco_py import MjSim, MjViewer, load_model_from_xml

from my_definitions import MODEL_XML
from my_utils import calculate_joints_values, get_joints_indices, load_xml_file

signal.signal(signal.SIGINT, signal.SIG_DFL)

if __name__ == "__main__":
    # ZeroMQ communication
    context = zmq.Context()

    print("Connecting to RPi to receive angles")
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://192.168.1.2:5555")
    socket.setsockopt(zmq.SUBSCRIBE, b"Angles")  # Choose topic to subscribe

    # MuJoCo simulation
    model = load_model_from_xml(load_xml_file(MODEL_XML))
    sim = MjSim(model)
    viewer = MjViewer(sim)

    joints_indices = get_joints_indices(sim)

    while True:
        frames: list = socket.recv_multipart()

        def _parse_frame_angles() -> list[float]:
            return list(map(float, str(frames[0])[2:-1].split(" ")[1:]))

        angles = _parse_frame_angles()
        joints = ["JOINT8E", "JOINT5E", "JOINT1E", "JOINT2E"]

        data = {}
        for (joint, angle) in zip(joints, angles):
            data[joint] = angle

        values = calculate_joints_values(data)

        sim_state = sim.get_state()
        for name, index in joints_indices.items():
            sim_state.qpos[index] = values[name]

        sim.set_state(sim_state)
        sim.forward()
        viewer.render()
